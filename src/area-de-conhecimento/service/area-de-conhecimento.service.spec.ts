import { Test, TestingModule } from '@nestjs/testing';
import { AreaDeConhecimentoService } from './area-de-conhecimento.service';

describe('AreaDeConhecimentoService', () => {
  let service: AreaDeConhecimentoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AreaDeConhecimentoService],
    }).compile();

    service = module.get<AreaDeConhecimentoService>(AreaDeConhecimentoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
