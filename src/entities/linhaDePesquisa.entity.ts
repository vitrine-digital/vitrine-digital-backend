import { Entity, Column, OneToMany } from 'typeorm';
import { SimpleEntity } from './simpleEntity.entity';
import { GrupoDePesquisa } from './grupoDePesquisa.entity';

@Entity()
export class LinhaDePesquisa extends SimpleEntity {
	@OneToMany(
		type => GrupoDePesquisa,
		grupoDePesquisa => grupoDePesquisa.linhaDePesquisa,
		{ cascade: true },
	)
	gruposDePesquisa: GrupoDePesquisa[];
}
