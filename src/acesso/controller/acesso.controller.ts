import { Param, ClassSerializerInterceptor, Controller, Post, UseInterceptors, NotFoundException } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Acesso } from 'src/entities/acesso.entity';
import { Produto } from 'src/entities/produto.entity';
import { ProdutoService } from 'src/produto/service/produto.service';
import { AcessoService } from '../service/acesso.service';

@ApiTags('Acessos')
@ApiBearerAuth()
@UseInterceptors(ClassSerializerInterceptor)
@Controller('acessos')
export class AcessoController {
    constructor(private service: AcessoService, private produtoService: ProdutoService) {}

    @Post(':id')
	@ApiOperation({ description: 'Salva um registro' })
	async saveOne(@Param('id') id: number): Promise<Acesso> {

		const produto: Produto = await this.produtoService.findOne(id);

		if(!produto) {
			throw new NotFoundException(`Este produto não existe na base de dados`)
		}

		return this.service.save({produto});
	}
}
