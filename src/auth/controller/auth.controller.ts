import {
	Controller,
	Post,
	Body,
	HttpStatus,
	UseGuards,
	Req,
	Res,
} from '@nestjs/common';
import { AuthService } from '../service/auth.service';
import { UsuarioService } from 'src/usuario/usuario.service';
import { Usuario } from 'src/entities/usuario.entity';
import {
	ApiTags,
	ApiOkResponse,
	ApiForbiddenResponse,
	ApiBearerAuth,
} from '@nestjs/swagger';
import { UsuarioLoginDto } from 'src/model/dto/dtoInput/usuarioLoginDto.dto';
import { Forbidden } from 'src/model/dto/dtoOutput/forbidden.dto';
import { UsuarioLoginDtoOutput } from 'src/model/dto/dtoOutput/usuarioLoginDtoOutput.dto';
import { JwtAuthGuard } from '../jwt-auth.guard';
import { RolesGuard } from '../role.guard';
import { Roles } from '../role.decorator';
import { Response } from 'express';

@ApiTags('Autenticação')
@ApiBearerAuth()
@Controller('auth')
export class AuthController {
	constructor(
		private readonly authService: AuthService,
		private readonly usuarioService: UsuarioService,
	) {}

	@ApiForbiddenResponse({ type: Forbidden })
	@ApiOkResponse({ type: UsuarioLoginDtoOutput })
	@Post('login')
	async loginUser(@Res() res: Response, @Body() body: UsuarioLoginDto) {
		if (!(body && body.email && body.senha)) {
			return res
				.status(HttpStatus.FORBIDDEN)
				.json({ message: 'Username and password are required!' });
		}

		const usuario = await this.usuarioService.getUsuarioByEmail(body.email);

		if (usuario) {
			if (
				await this.usuarioService.compareHash(body.senha, usuario.senha)
			) {
				return res
					.status(HttpStatus.OK)
					.json(await this.authService.createToken(usuario));
			}
		}

		return res
			.status(HttpStatus.FORBIDDEN)
			.json({ message: 'Username or password wrong!' });
	}

	@Post('register')
	async registerUser(@Res() res: Response, @Body() body: Usuario) {
		if (!(body && body.email && body.senha)) {
			return res
				.status(HttpStatus.FORBIDDEN)
				.json({ message: 'Username and password are required!' });
		}

		let usuario = await this.usuarioService.getUsuarioByEmail(body.email);

		if (usuario) {
			return res
				.status(HttpStatus.FORBIDDEN)
				.json({ message: 'Username exists' });
		} else {
			usuario = await this.usuarioService.createUsuario(body);
		}

		return res.status(HttpStatus.OK).json(usuario);
	}

	@ApiForbiddenResponse({ type: Forbidden })
	@ApiOkResponse({ type: UsuarioLoginDtoOutput })
	@Post('check')
	@UseGuards(JwtAuthGuard)
	async check(@Req() req, @Res() res: Response) {
		if (req.user) {
			const usuario = await this.usuarioService.getUsuarioById(
				req.user.id,
			);

			if (usuario) {
				return res
					.status(HttpStatus.OK)
					.json(await this.authService.createToken(usuario));
			} else {
				return res
					.status(HttpStatus.UNAUTHORIZED)
					.json({ message: 'Usuário não encontrado' });
			}
		}

		return res
			.status(HttpStatus.FORBIDDEN)
			.json({ message: 'Token não recebido' });
	}
}
