import {
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  Column,
  BaseEntity,
} from 'typeorm';
import { ApiHideProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export abstract class AbstractEntity extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: true })
  ativo: boolean;

  @Exclude()
  @ApiHideProperty()
  @CreateDateColumn()
  createdAt?: string;

  @Exclude()
  @ApiHideProperty()
  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt?: number;
}
