import { Module, forwardRef } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigService, ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LinhaDePesquisaModule } from './linha-de-pesquisa/linha-de-pesquisa.module';
import { GrupoDePesquisaModule } from './grupo-de-pesquisa/grupo-de-pesquisa.module';
import { UsuarioModule } from './usuario/usuario.module';
import { AuthModule } from './auth/auth.module';
import { AreaDeConhecimentoModule } from './area-de-conhecimento/area-de-conhecimento.module';
import { ProdutoModule } from './produto/produto.module';
import { FilesModule } from './files/files.module';
import { MongooseModule } from '@nestjs/mongoose';
import { PalavraChaveModule } from './palavra-chave/palavra-chave.module';
import databaseConfig from './config/database';
import { GraphQLModule } from '@nestjs/graphql';
import { AcessoModule } from './acesso/acesso.module';
import { RelatorioModule } from './relatorio/relatorio.module';
import { ProdutoSolicitacaoModule } from './produto-solicitacao/produto-solicitacao.module';

@Module({
	imports: [
		ConfigService,
		forwardRef(() => ConfigModule.forRoot({ load: [databaseConfig] })),
		TypeOrmModule.forRootAsync({
			imports: [ConfigModule],
			useFactory: (config: ConfigService) => config.get('database'),
			inject: [ConfigService],
		}),
		MongooseModule.forRoot(
			process.env.MONGO_INITDB_ROOT_USERNAME &&
				process.env.MONGO_INITDB_ROOT_PASSWORD
				? `mongodb://vitrine-digital-storage:27017`
				: 'mongodb://localhost:27017/vitrine',
			{
				db: 'vitrine',
				dbName: 'vitrine',
				connectionName: 'files',
				user: process.env.MONGO_INITDB_ROOT_USERNAME,
				pass: process.env.MONGO_INITDB_ROOT_PASSWORD,
				useNewUrlParser: true,
				useUnifiedTopology: true,
			},
		),
		GraphQLModule.forRoot({
			autoSchemaFile: true,
			buildSchemaOptions: {
				dateScalarMode: 'timestamp',
			}
		}),
		LinhaDePesquisaModule,
		GrupoDePesquisaModule,
		UsuarioModule,
		AuthModule,
		AreaDeConhecimentoModule,
		ProdutoModule,
		// https://medium.com/@khoa.phan.9xset/nestjs-file-uploading-using-multer-gridfs-7569a1b48022
		FilesModule,
		PalavraChaveModule,
		AcessoModule,
		RelatorioModule,
		ProdutoSolicitacaoModule
	],
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule {}
