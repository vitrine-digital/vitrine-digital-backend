import { Test, TestingModule } from '@nestjs/testing';
import { LinhaDePesquisaService } from './linha-de-pesquisa.service';

describe('LinhaDePesquisaService', () => {
  let service: LinhaDePesquisaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LinhaDePesquisaService],
    }).compile();

    service = module.get<LinhaDePesquisaService>(LinhaDePesquisaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
