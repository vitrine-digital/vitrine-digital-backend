import {
	Controller,
	Get,
	Query,
	Post,
	UseGuards,
	Body,
	Param,
	UseInterceptors,
	UploadedFiles,
	Put,
	Request,
	ClassSerializerInterceptor,
} from '@nestjs/common';
import { ProdutoService } from '../service/produto.service';
import {
	ApiOperation,
	ApiTags,
	ApiBearerAuth,
	ApiConsumes,
} from '@nestjs/swagger';
import { QueryOptions } from 'src/model/queryOptions.dto';
import { SearchProduto } from 'src/model/dto/search/searchProduto.dto';
import { Pagination } from 'nestjs-typeorm-paginate';
import { Produto } from 'src/entities/produto.entity';
import { removePagination } from 'src/_helpers/utils';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { RolesGuard } from 'src/auth/role.guard';
import { Roles } from 'src/auth/role.decorator';
import { ProdutoDtoInput } from 'src/model/dto/dtoInput/produto';
import { FilesInterceptor } from '@nestjs/platform-express';
import { FileUpload } from 'src/model/fileUpload.dto';
import { PalavraChaveService } from 'src/palavra-chave/service/palavra-chave.service';
import { PalavraChave } from 'src/entities/palavraChave.entity';
import { AddUserGuard } from 'src/_helpers/add-user.guard';

@ApiTags('Produto')
@ApiBearerAuth()
@Controller('produtos')
@UseInterceptors(ClassSerializerInterceptor)
export class ProdutoController {
	constructor(
		private service: ProdutoService,
		private palavraChaveService: PalavraChaveService,
	) {}

	@Get()
	@UseGuards(AddUserGuard)
	@ApiOperation({ description: 'Retorna uma listagem de itens' })
	getAll(
		@Query() options: QueryOptions,
		@Query() search: SearchProduto,
		@Request() req,
	): Promise<Pagination<Produto>> {
		if (req.user && !search.all) {
			search.solicitanteId = req.user.id;
		}

		const page = this.service.getAll(
			{ ...options, route: 'teste' },
			removePagination(search),
		);

		return page.then(res => {
			res.items.forEach(item => {
				delete item.proprietario.senha;
			});
			return res;
		}); // TODO: Trocar pela string de rota certa.
	}

	@Get(':id')
	@ApiOperation({ description: 'Retorna uma listagem de itens' })
	findOne(@Param('id') id: number): Promise<Produto> {
		return this.service.findOne(id);
	}

	@Put(':id')
	@ApiOperation({ description: 'Atualiza um item' })
	atualizar(
		@Param('id') id: number,
		@Body() input: Produto,
	): Promise<Produto> {
		return this.service.saveOne(input);
	}

	@Post()
	@UseGuards(JwtAuthGuard, RolesGuard)
	@Roles('admin')
	@ApiOperation({ description: 'Salva um registro' })
	async saveOne(@Body() input: ProdutoDtoInput): Promise<Produto> {
		if (!input.paragrafos) {
			input.paragrafos = [];
		}
		if (!input.videos) {
			input.videos = [];
		}
		if (!input.palavrasChave) {
			input.palavrasChave = [];
		}
		input.palavrasChave.forEach(item =>
			this.handlePersistPalavraChave(item),
		);
		return this.service.saveOne(input);
	}

	async handlePersistPalavraChave(palavra: PalavraChave) {
		if (!palavra.id) {
			const palavraBase = this.palavraChaveService.findOneByNome(
				palavra.nome,
			);
			if (!palavraBase) {
				palavra.id = (await this.palavraChaveService.save(palavra)).id;
			}
		}
	}

	@Post(':id/imgs')
	@ApiConsumes('multipart/form-data')
	@UseInterceptors(FilesInterceptor('files'))
	async upload(
		@Body() input: FileUpload,
		@UploadedFiles() files,
		@Param('id') id: number,
	) {
		const response = [];
		files.forEach(file => {
			const fileReponse = {
				originalname: file.originalname,
				encoding: file.encoding,
				mimetype: file.mimetype,
				id: file.id,
				filename: file.filename,
				metadata: file.metadata,
				bucketName: file.bucketName,
				chunkSize: file.chunkSize,
				size: file.size,
				md5: file.md5,
				uploadDate: file.uploadDate,
				contentType: file.contentType,
			};
			response.push(fileReponse);
		});
		const imagens: any[] = response.map(item => ({
			imagem: `${item.id}`,
			produto: { id },
			nome: item.filename,
		}));
		await this.service.saveImagem(imagens);
		return this.service.findOne(id);
	}
}
