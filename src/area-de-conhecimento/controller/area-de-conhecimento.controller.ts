import { Controller, ClassSerializerInterceptor, UseInterceptors, Get, Query, Post, UseGuards, Body, Put, Param } from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiOperation } from '@nestjs/swagger';
import { AreaDeConhecimentoService } from '../service/area-de-conhecimento.service';
import { QueryOptions } from 'src/model/queryOptions.dto';
import { SearchGeneric } from 'src/model/dto/search/searchGeneric.dto';
import { Pagination } from 'nestjs-typeorm-paginate';
import { AreaDeConhecimento } from 'src/entities/areaDeConhecimento.entity';
import { removePagination } from 'src/_helpers/utils';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { RolesGuard } from 'src/auth/role.guard';
import { Roles } from 'src/auth/role.decorator';
import { SimpleEntityDTO } from 'src/model/dto/simpleEntityDTO.dto';

@ApiTags('Área de Conhecimento')
@ApiBearerAuth()
@UseInterceptors(ClassSerializerInterceptor)
@Controller('areas-de-conhecimento')
export class AreaDeConhecimentoController {
    constructor(private service: AreaDeConhecimentoService) {}

    @Get()
    @ApiOperation({description: 'Retorna uma listagem de itens'})
	getAll(@Query() options: QueryOptions, @Query() search: SearchGeneric): Promise<Pagination<AreaDeConhecimento>> {
		return this.service.getAll({...options, route: 'teste' }, removePagination(search)); // TODO: Trocar pela string de rota certa.
    }

    @Post()
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('admin')
    @ApiOperation({description: 'Salva um registro'})
    saveOne(@Body() input: SimpleEntityDTO): Promise<AreaDeConhecimento> {
        return this.service.saveOne(input);
    }

    @Put(':id')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles('admin')
    @ApiOperation({description: 'Altera um registro'})
    updateOne(@Param('id') id: number, @Body() input: SimpleEntityDTO) {
        return this.service.update(id, input);
    }
}
