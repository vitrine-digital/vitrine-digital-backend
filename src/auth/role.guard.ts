import { Injectable, CanActivate, ExecutionContext, Inject } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UsuarioService } from 'src/usuario/usuario.service';
import { Usuario } from 'src/entities/usuario.entity';

@Injectable()
export class RolesGuard implements CanActivate {
	constructor(private reflector: Reflector, @Inject('UsuarioService') private usuarioSerice: UsuarioService) {}

	async canActivate(context: ExecutionContext): Promise<boolean> {
		const roles = this.reflector.get<string[]>(
			'roles',
			context.getHandler(),
		);
		if (!roles) {
			return true;
        }
		const request = context.switchToHttp().getRequest();
		const usuarioRequest: Usuario = request.user;
		const usuario: Usuario = await this.usuarioSerice.getUsuarioById(usuarioRequest.id)
		return this.matchRoles(roles, ['user.roles']);
    }
    
    matchRoles(roles: string[], userRoles: string[]) {
		// TODO: Adicionar lógica de verificação de role.
        return true;
    }
}
