import { ConnectionOptions } from "typeorm";

const config: ConnectionOptions = {
    type: "mysql",
    host: process.env.TYPEORM_HOST || 'localhost',
    port: parseInt(process.env.TYPEORM_PORT) || 3306,
    username: process.env.TYPEORM_USER || 'root',
    password: process.env.TYPEORM_PASSWORD || 'root',
    database: process.env.TYPEORM_DATABASE || 'vitrine',
    entities: [__dirname + '/../**/*.entity{.ts,.js}'],
    synchronize: !!process.env.TYPEORM_SYNCHRONIZE || false,
    migrationsTableName: "migrations_typeorm",
    migrationsRun: true,
    migrations: [__dirname + '/../migration/*{.ts,.js}'],

    cli: {
        migrationsDir: '../migration',
    },

    ssl: false,
}

export = config;