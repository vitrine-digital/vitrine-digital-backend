import { BaseEntity, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Produto } from "./produto.entity";

@Entity()
export class Acesso extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(
		type => Produto,
		produto => produto.acessos,
		{ eager: true },
	)
    produto: Produto;
    
    // https://dev.mysql.com/doc/refman/8.0/en/data-type-defaults.html#:~:text=If%20a%20data%20type%20specification,with%20no%20explicit%20DEFAULT%20clause.
    // https://github.com/typeorm/typeorm/issues/877#issuecomment-329148819
    @CreateDateColumn()
    createdAt?: string;
}