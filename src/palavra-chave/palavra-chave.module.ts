import { Module } from '@nestjs/common';
import { PalavraChaveService } from './service/palavra-chave.service';
import { PalavraChaveController } from './controller/palavra-chave.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PalavraChave } from 'src/entities/palavraChave.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PalavraChave])],
  providers: [PalavraChaveService],
  controllers: [PalavraChaveController],
  exports: [PalavraChaveService]
})
export class PalavraChaveModule {}
