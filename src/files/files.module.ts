import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { GridFsMulterConfigService } from './GridFsConfiguration.service';
import { FilesController } from './controller/files.controller';
import { FilesService } from './service/files.service';

@Module({
	imports: [
		MulterModule.registerAsync({
			useClass: GridFsMulterConfigService,
		}),
		FilesService
	],
	controllers: [FilesController],
	providers: [GridFsMulterConfigService, FilesService],
	exports: [FilesService]
})
export class FilesModule {}
