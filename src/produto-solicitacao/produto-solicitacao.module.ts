import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProdutoSolicitacao } from 'src/entities/produtoSolicitacao.entity';
import { ProdutoSolicitacaoService } from './service/produto-solicitacao.service';
import { ProdutoSolicitacaoController } from './controller/produto-solicitacao.controller';

@Module({
  imports: [TypeOrmModule.forFeature([ProdutoSolicitacao])],
  providers: [ProdutoSolicitacaoService],
  controllers: [ProdutoSolicitacaoController]
})
export class ProdutoSolicitacaoModule {}
