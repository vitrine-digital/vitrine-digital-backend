import { Resolver, Query, Args } from '@nestjs/graphql';
import { Produto } from 'src/entities/produto.entity';
import { MostAccessInput } from 'src/graphql/inputs/mostAccessInput';
import { PaginatedProduto } from 'src/model/paginatedModels';
import { ProdutoService } from 'src/produto/service/produto.service';

@Resolver(of => Produto)
export class RelatorioResolver {
    constructor(
        private readonly produtoService: ProdutoService
    ) {}

    @Query(returns => PaginatedProduto)
    async maisAcessados(@Args('mostAccessData') mostAccessData: MostAccessInput) {
        return this.produtoService.getMostAccess(mostAccessData);
    }
}