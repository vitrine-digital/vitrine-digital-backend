import { Module } from '@nestjs/common';
import { AreaDeConhecimentoController } from './controller/area-de-conhecimento.controller';
import { AreaDeConhecimentoService } from './service/area-de-conhecimento.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AreaDeConhecimento } from 'src/entities/areaDeConhecimento.entity';

@Module({
  imports: [TypeOrmModule.forFeature([AreaDeConhecimento])],
  controllers: [AreaDeConhecimentoController],
  providers: [AreaDeConhecimentoService]
})
export class AreaDeConhecimentoModule {}
