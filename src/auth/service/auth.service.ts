import { Injectable } from '@nestjs/common';
import { UsuarioService } from 'src/usuario/usuario.service';
import * as jwt from 'jsonwebtoken';
import { Usuario } from 'src/entities/usuario.entity';
import { UsuarioDtoOutput } from 'src/model/dto/dtoOutput/usuarioDtoOutput.dto';
import { classToClass } from 'class-transformer';

@Injectable()
export class AuthService {
	constructor(private usuarioService: UsuarioService) {}

	async createToken(usuario: Usuario) {
		const expiresIn = 60 * 60;
		const secretOrKey = 'secret'; //TODO: Adicionar propriedade no .env.
		const payload = {
			id: usuario.id,
			nome: usuario.nome,
			email: usuario.email,
		};
		const token = jwt.sign(payload, secretOrKey, { expiresIn });

		const user: UsuarioDtoOutput = classToClass(usuario);

		return { usuario: user, expires_in: expiresIn, token };
	}

	async validateUser(usuario: Usuario): Promise<boolean> {
		if (usuario && usuario.email) {
			return Boolean(
				this.usuarioService.getUsuarioByEmail(usuario.email),
			);
		}

		return false;
	}
}
