import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AreaDeConhecimento } from 'src/entities/areaDeConhecimento.entity';
import { IPaginationOptions, Pagination, paginate } from 'nestjs-typeorm-paginate';
import { _Like } from 'src/_helpers/utils';
import { SimpleEntityDTO } from 'src/model/dto/simpleEntityDTO.dto';

@Injectable()
export class AreaDeConhecimentoService {
    constructor(@InjectRepository(AreaDeConhecimento) private repository: Repository<AreaDeConhecimento>){}

    async getAll(
		options: IPaginationOptions,
		search: Partial<SimpleEntityDTO>,
	): Promise<Pagination<AreaDeConhecimento>> {
		const queryBuilder = this.repository.createQueryBuilder();
		if (Object.entries(search).length !== 0) {
			queryBuilder.where({
				...search,
				nome: _Like(search.nome),
				descricao: _Like(search.descricao),
			});
		}
		return paginate<AreaDeConhecimento>(queryBuilder, options);
	}

  async saveOne(input: SimpleEntityDTO): Promise<AreaDeConhecimento> {
    return this.repository.save(input);
  }

  async update(id: number, input: SimpleEntityDTO){
    return this.repository.update(id, input);
}
}
