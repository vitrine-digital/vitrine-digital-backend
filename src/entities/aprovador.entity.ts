import { Column, Entity, JoinColumn, ManyToOne, OneToOne } from "typeorm";
import { AbstractEntity } from "./abstractEntity.entity";
import { ProdutoSolicitacao } from "./produtoSolicitacao.entity";
import { Usuario } from "./usuario.entity";

@Entity()
export class Aprovador extends AbstractEntity {
    @OneToOne(() => Usuario)
    @JoinColumn()
    usuario: Usuario;

    @Column({default: null})
    aprovado: Boolean;

    @ManyToOne(
        type => ProdutoSolicitacao,
		produtoSolicitacao => produtoSolicitacao.aprovadores
    )
    solicitacao: ProdutoSolicitacao;
}