import { Entity } from 'typeorm';
import { SimpleEntity } from './simpleEntity.entity';

@Entity()
export class AreaDeConhecimento extends SimpleEntity {}
