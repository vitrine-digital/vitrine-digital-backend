import { Test, TestingModule } from '@nestjs/testing';
import { AreaDeConhecimentoController } from './area-de-conhecimento.controller';

describe('AreaDeConhecimento Controller', () => {
  let controller: AreaDeConhecimentoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AreaDeConhecimentoController],
    }).compile();

    controller = module.get<AreaDeConhecimentoController>(AreaDeConhecimentoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
