import { FileInfoVm } from './fileInfo.dto';
import { ApiProperty } from '@nestjs/swagger';


export class FileResponseVm {
    message: string;

    @ApiProperty({ type: FileInfoVm })
    file: FileInfoVm;
}