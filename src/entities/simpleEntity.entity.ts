import { AbstractEntity } from './abstractEntity.entity';
import { Column } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export abstract class SimpleEntity extends AbstractEntity {
	@Field()
	@Column()
	nome: string;

	@Field()
	@Column()
	descricao: string;
}
