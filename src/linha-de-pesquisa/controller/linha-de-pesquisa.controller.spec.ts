import { Test, TestingModule } from '@nestjs/testing';
import { LinhaDePesquisaController } from './linha-de-pesquisa.controller';

describe('LinhaDePesquisa Controller', () => {
  let controller: LinhaDePesquisaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LinhaDePesquisaController],
    }).compile();

    controller = module.get<LinhaDePesquisaController>(LinhaDePesquisaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
