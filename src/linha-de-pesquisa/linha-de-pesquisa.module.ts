import { Module } from '@nestjs/common';
import { LinhaDePesquisaController } from './controller/linha-de-pesquisa.controller';
import { LinhaDePesquisaService } from './service/linha-de-pesquisa.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LinhaDePesquisa } from 'src/entities/linhaDePesquisa.entity';

@Module({
  imports: [TypeOrmModule.forFeature([LinhaDePesquisa])],
  controllers: [LinhaDePesquisaController],
  providers: [LinhaDePesquisaService]
})
export class LinhaDePesquisaModule {}
