import { SimpleEntityDTO } from "../simpleEntityDTO.dto";
import { GrupoDePesquisaDtoInput } from "./grupoDePesquisaDtoInput.dto";
import { Usuario } from "src/entities/usuario.entity";
import { FichaTecnica } from "src/entities/fichaTecnica";
import { PalavraChave } from "src/entities/palavraChave.entity";

export class ProdutoDtoInput extends SimpleEntityDTO {
	titulo: string;
	site?: string;
	fotos?: Object;
	paragrafos?: Object;
	palavrasChave?: PalavraChave[];
	videos?: Object;
	grupoDePesquisa: GrupoDePesquisaDtoInput;
	proprietario?: Usuario;
	fichaTecnica?: FichaTecnica;
}