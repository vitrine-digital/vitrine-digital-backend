import { Controller, Post, Get, Param, HttpException, HttpStatus, Res, UploadedFiles, UseInterceptors, Body } from '@nestjs/common';
import { FilesService } from '../service/files.service';
import { FilesInterceptor } from '@nestjs/platform-express';
import {
	ApiCreatedResponse,
	ApiConsumes,
	ApiBadRequestResponse,
    ApiInternalServerErrorResponse,
    ApiTags
} from '@nestjs/swagger';
import { FileResponseVm } from 'src/model/fileResponse.dto';
import { FileUpload } from 'src/model/fileUpload.dto';

@ApiTags('File Upload')
@Controller('files')
export class FilesController {
	constructor(private filesService: FilesService) {}
	@Post('')
	@ApiConsumes('multipart/form-data')
	@UseInterceptors(FilesInterceptor('files'))
	upload(@Body() input: FileUpload, @UploadedFiles() files) {
		const response = [];
		files.forEach(file => {
			const fileReponse = {
				originalname: file.originalname,
				encoding: file.encoding,
				mimetype: file.mimetype,
				id: file.id,
				filename: file.filename,
				metadata: file.metadata,
				bucketName: file.bucketName,
				chunkSize: file.chunkSize,
				size: file.size,
				md5: file.md5,
				uploadDate: file.uploadDate,
				contentType: file.contentType,
			};
			response.push(fileReponse);
		});
		return response;
	}

	@Get('info/:id')
	@ApiBadRequestResponse({ type: ApiInternalServerErrorResponse })
	async getFileInfo(@Param('id') id: string): Promise<FileResponseVm> {
		const file = await this.filesService.findInfo(id);
		const filestream = await this.filesService.readStream(id);
		if (!filestream) {
			throw new HttpException(
				'An error occurred while retrieving file info',
				HttpStatus.EXPECTATION_FAILED,
			);
		}
		return {
			message: 'File has been detected',
			file: file,
		};
	}

	@Get(':id')
	@ApiBadRequestResponse({ type: ApiInternalServerErrorResponse })
	async getFile(@Param('id') id: string, @Res() res) {
		const file = await this.filesService.findInfo(id);
		const filestream = await this.filesService.readStream(id);
		if (!filestream) {
			throw new HttpException(
				'An error occurred while retrieving file',
				HttpStatus.EXPECTATION_FAILED,
			);
		}
		res.header('Content-Type', file.contentType);
		return filestream.pipe(res);
	}

	@Get('download/:id')
	@ApiBadRequestResponse({ type: ApiInternalServerErrorResponse })
	async downloadFile(@Param('id') id: string, @Res() res) {
		const file = await this.filesService.findInfo(id);
		const filestream = await this.filesService.readStream(id);
		if (!filestream) {
			throw new HttpException(
				'An error occurred while retrieving file',
				HttpStatus.EXPECTATION_FAILED,
			);
		}
		res.header('Content-Type', file.contentType);
		res.header(
			'Content-Disposition',
			'attachment; filename=' + file.filename,
		);
		return filestream.pipe(res);
	}

	@Get('delete/:id')
	@ApiBadRequestResponse({ type: ApiInternalServerErrorResponse })
	@ApiCreatedResponse({ type: FileResponseVm })
	async deleteFile(@Param('id') id: string): Promise<FileResponseVm> {
		const file = await this.filesService.findInfo(id);
		const filestream = await this.filesService.deleteFile(id);
		if (!filestream) {
			throw new HttpException(
				'An error occurred during file deletion',
				HttpStatus.EXPECTATION_FAILED,
			);
		}
		return {
			message: 'File has been deleted',
			file: file,
		};
	}
}
