import { Test, TestingModule } from '@nestjs/testing';
import { ProdutoSolicitacaoController } from './produto-solicitacao.controller';

describe('ProdutoSolicitacao Controller', () => {
  let controller: ProdutoSolicitacaoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProdutoSolicitacaoController],
    }).compile();

    controller = module.get<ProdutoSolicitacaoController>(ProdutoSolicitacaoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
